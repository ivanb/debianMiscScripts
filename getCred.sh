#!/bin/sh

# Intellectual property information START
# 
# Copyright (c) 2022 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
# 
# The script will get the information from sqlite3 database.
# 
# Description END

# How sql string should look like inside sqlite3 shell:
# SELECT site, login, passwd FROM cred WHERE site LIKE '%NFS%';

theFile='/media/ivanb/ToshibaP300/localAdm.db'
> /dev/null 2>&1 ls "${theFile}"
if test $? != 0
then
  echo ''
  echo 'Drive is not mounted!'
  echo ''
  exit 1
fi

optString='.mode line'
sqlString='SELECT site, login, passwd FROM cred;'
site='*'

clear
read -p "Site's name: " site

if test $? != 0
then
  site='*'
fi

if test "$site" != '*'
then
  sqlString="${sqlString%;} WHERE site LIKE '%${site}%';"
fi

echo ''
sqlite3 "$theFile" "$optString" "$sqlString"
echo ''

read -p 'Press RETURN to clear clipboard...' theEC

xsel -pc
xsel -sc
xsel -bc

clear
# END OF SCRIPT

